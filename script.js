"use strict"

/* Завдання 1.
   Напишіть функцію, яка повертає частку двох чисел.
   Виведіть результат роботи функції в консоль.
*/

function divideTwoNumbers (number1, number2) {
    return number1 / number2;
}
console.log(divideTwoNumbers(10,2));


/* 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера два числа.
- Провалідувати отримані значення(перевірити, що отримано числа).
- Якщо користувач ввів не число, запитувати до тих пір, поки не введе число.

- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
- Сюди може бути введено +, -, *, /. Провалідувати отримане значення.
- Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').

- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.
*/

let userNumber1 = prompt('Введіть перше число:');
while (userNumber1 === null || userNumber1 === '' || isNaN(userNumber1)) {
    userNumber1 = prompt('Введіть перше число:');
}
let userNumber2 = prompt('Введіть друге число:');
while (userNumber2 === null || userNumber2 === '' || isNaN(userNumber2)) {
    userNumber2 = prompt('Введіть друге число:');
}
let mathOperation = prompt('Введіть математичну операцію, що треба виконати в форматі +, -, *, /');
if (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '*' && mathOperation !== '/') {
    alert('Такої операції не існує');
}

function calculateNumbers (number1, number2, operation) {
    switch (operation) {
        case '+':
            return Number(number1) + Number(number2);
        case '-':
            return number1 - number2;
        case '*':
            return number1 * number2;
        case '/':
            return number1 / number2;
    }
}

console.log(calculateNumbers(userNumber1, userNumber2, mathOperation));

/*
3. Опціонально. Завдання:
Реалізувати функцію підрахунку факторіалу числа.
Технічні вимоги:
- Отримати за допомогою модального вікна браузера число, яке введе користувач.
- За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
- Використовувати синтаксис ES6 для роботи зі змінними та функціями.

5! = 1*2*3*4*5 =
5! = 1 (1 + 1) * (2 + 1)
 */



const calculateFactorial = function (value) {
    if (value > 0) {
        let result = 1;
        for (let i = 1; i <= value; i++) {
            result *= i;
        }
        return result;
    } else if (+value === 0) {
        return 0;
    } else {
        alert("Введене число не може бути від'ємним");
    }
}

const anotherUserNumber = prompt('Введіть число');
if (anotherUserNumber !== null && anotherUserNumber !== '' && !isNaN(anotherUserNumber)) {
    console.log(calculateFactorial(anotherUserNumber));
} else {
    alert('Введено не число');
}
